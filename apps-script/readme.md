## Importazione dei dati in un Google Spreadsheet

 - Creare un [nuovo google spreadsheet](https://spreadsheet.new/)
 - Creare un foglio `Configurazione` e importarci dentro il file csv [Configurazione.csv](Configurazione.csv)
 - Creare un nuovo Apps Script da Estensioni > Apps Script
 - Aggiungere i file [Main.gs](Main.gs) e [UrlGenerator.gs](UrlGenerator.gs)
 - Ricaricare il google spreadsheet
 - Inserire le configurazioni
 - Cliccare sul menu Opencity Italia per aggiornare i dati. 
 - Quando lo script ha completato la sua esecuzione è possibile visualizzare i dati nel foglio Dati