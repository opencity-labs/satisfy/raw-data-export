//////////////////////////////////////////////////////////////////////////
//////////////////////////// start config ////////////////////////////////

const configSheetName = 'Configurazione';

const dataSheetName = 'Dati';

const questionsAndAnswersSheetName = 'Domande e risposte';

const configMapper = {
  'tenant-id': 'B4',
  'start-year': 'B5',
  'start-month': 'B6',
  'AWS_KEY': 'B7',
  'AWS_SECRET': 'B8',
  'REGION': 'B9',
  'BUCKET': 'B10',
  'PREFIX': 'B11',
}

const satisfyApiBaseUrl = 'https://satisfy.opencityitalia.it/api/rest';

const delimiter = ",";

const importCsvHeaders = true;

const verbose = true;

////////////////////////////// end config ////////////////////////////////
//////////////////////////////////////////////////////////////////////////

//@OnlyCurrentDoc
function onOpen(e) {
  const ui = SpreadsheetApp.getUi();
  ui.createMenu("Opencity Italia")
    .addItem("Aggiorna dati Satisfy", "importData")
    //    .addItem("Aggiorna domande e risposte", "importQuestionsAndAnswers")
    .addItem("Rimuovi dati", "removeData")
    .addToUi();
}

//Displays a text as a Toast message
function displayLog(message, title = "Log di esecuzione") {
  if (verbose) {
    SpreadsheetApp.getActive().toast(message, title, -1);
  }
}

let configCache = {};
function getConfig(name) {
  const spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName(configSheetName), true);
  if (configMapper.hasOwnProperty(name)) {
    if (configCache.hasOwnProperty(name)) {
      return configCache[name];
    }
    spreadsheet.getRange(configMapper[name]).activate();
    value = spreadsheet.getCurrentCell().getValue() + "";
    if (value.length === 0) {
      throw new Error('Missing config ' + name);
    }
    configCache[name] = value;
    return value;
  }
  return null;
}

function setConfig(name, value) {
  const spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName(configSheetName), true);
  if (configMapper.hasOwnProperty(name)) {
    spreadsheet.getRange(configMapper[name]).activate();
    value = spreadsheet.getCurrentCell().setValue(value)
  }
}

function getEntrypointsByTenant(tenantId) {
  const url = satisfyApiBaseUrl + '/entrypoints-by-tenant/' + tenantId;
  try {
    const response = UrlFetchApp.fetch(url);
    return JSON.parse(response.getContentText());
  } catch (f) {
    displayLog(f.message, 'Errore');
  }
  return { 'entrypoints': [] };
}

let headers = [];
function getHeaders() {
  if (headers.length === 0) {
    const url = getS3SignedGetUrl('headers.csv');
    headers = Utilities.parseCsv(UrlFetchApp.fetch(url), delimiter);
    headers[0].unshift('entrypoint');
  }
  return headers;
}

function getDataSheet(createIfNotExists) {
  const spreadsheet = SpreadsheetApp.getActive();
  let sheetName = dataSheetName;
  let sheet = spreadsheet.getSheetByName(sheetName);
  if (!sheet) {
    if (!createIfNotExists) {
      return false;
    }
    sheet = spreadsheet.insertSheet(sheetName);
    spreadsheet.setActiveSheet(sheet, true);
    if (importCsvHeaders) {
      let csvHeaders = getHeaders();
      sheet.getRange(1, 1, 1, csvHeaders[0].length).setValues(csvHeaders);
    }
  } else {
    spreadsheet.setActiveSheet(sheet, true);
  }

  return sheet;
}

function removeData() {
  const spreadsheet = SpreadsheetApp.getActive();
  const tenantId = getConfig('tenant-id');
  const entrypoints = getEntrypointsByTenant(tenantId).entrypoints;
  let sheet;
  entrypoints.forEach(function (element) {
    sheet = spreadsheet.getSheetByName(element.name);
    if (sheet) {
      spreadsheet.deleteSheet(sheet)
    }
  });
  sheet = spreadsheet.getSheetByName(dataSheetName);
  if (sheet) {
    sheet.getRange('A2:N').clearContent();
  }
}

function getStartYear() {
  return parseInt(getConfig('start-year'))
}

function getStartMonth() {
  return parseInt(getConfig('start-month'))
}

function importData() {
  const tenantId = getConfig('tenant-id');
  const entrypoints = getEntrypointsByTenant(tenantId).entrypoints;

  const currentYear = new Date().getFullYear();
  const currentMonth = new Date().getMonth() + 1;
  const startDate = {
    year: getStartYear(),
    month: getStartMonth()
  };

  let dates = [];
  let fileIdList = [];
  for (let i = startDate.year; i <= currentYear; i++) {
    for (let j = 1; j <= 12; j++) {
      if (i === startDate.year && j < startDate.month) {
        continue;
      }
      if (i === currentYear && j > currentMonth) {
        continue;
      }
      dates.push({
        year: i,
        month: j
      });
      fileIdList.push(i.toString() + '-' + j.toString().padStart(2, '0'))
    }
  }

  SpreadsheetApp.getActive().setActiveSheet(getDataSheet(true), true);
  removeRowsByDates(fileIdList);
  dates.forEach(function (date) {
    displayLog('Ricerca dei dati per il periodo ' + date.year + '/' + date.month, 'Importazione dati');
    loopEntrypointsByDate(entrypoints, date.year, date.month);
  })
  displayLog('', 'Importazione dati completata');
}

function removeRowsByDates(fileIdList) {
  var sheet = getDataSheet(true);
  var rows = sheet.getDataRange();
  var numRows = rows.getNumRows();
  var values = rows.getDisplayValues();

  var rowsDeleted = 0;
  for (var i = 0; i <= numRows - 1; i++) {
    var row = values[i];
    if (fileIdList.includes(row[1])) {
      sheet.deleteRow((parseInt(i) + 1) - rowsDeleted);
      rowsDeleted++;
    }
  }
}

function loopEntrypointsByDate(entrypoints, year, month) {
  setConfig('start-year', year);
  setConfig('start-month', month)
  let date = year.toString() + '-' + month.toString().padStart(2, '0');
  entrypoints.forEach(function (entrypoint) {
    let filename = PREFIX + '/' + entrypoint.id + '/' + date + '.csv';
    try {
      importCSV(filename, entrypoint);
    } catch (Error) {
      Logger.log('Fail fetching ' + filename)
    }
  });
}

function importCSV(filename, entrypoint) {
  const url = getS3SignedGetUrl(filename);
  const contents = Utilities.parseCsv(UrlFetchApp.fetch(url), delimiter);
  if (contents.length > 0) {
    Logger.log('Import file ' + filename)
    var sheet = getDataSheet(true);

    let tsIndex = 1; //@todo find timestamp index
    let questionIndex = 11; //@todo find question index
    let answerIndex = 12; //@todo find answer index   
    let uaIndex = 6; //@todo find userAgent index    
    for (let i = 0; i < contents.length; i++) {
      contents[i][tsIndex] = Utilities.parseDate(contents[i][tsIndex], "GMT", "yyyy-MM-dd'T'HH:mm:ss.S'Z'"); // format timestamps 2023-10-12T10:26:47.175Z      
      contents[i][questionIndex] = getQuestionAnswerText(contents[i][questionIndex]);
      contents[i][answerIndex] = getQuestionAnswerText(contents[i][answerIndex]);
    }

    for (let i = 0; i < contents.length; i++) {
      contents[i].unshift(entrypoint.name);
    }
    const sheetName = writeDataToSheet(contents, sheet);
    Logger.log("Il file csv " + filename + " è stato importato nel foglio " + sheetName + " per entrypoint " + entrypoint.name);
    if (importCsvHeaders) {
      sheet.setFrozenRows(1);
    }
    sheet.sort(2)
  }
}

function writeDataToSheet(data, sheet) {
  let startRow = sheet.getLastRow() + 1;
  let startCol = 1;
  // Determines the incoming data size.
  let numRows = data.length;
  let numColumns = data[0].length;

  // Appends data into the sheet.
  sheet.getRange(startRow, startCol, numRows, numColumns).setValues(data);
  return sheet.getName();
}

let questionsAndAnswersCache = [];
function getQuestionsAndAnswers() {
  if (questionsAndAnswersCache.length > 0) {
    return questionsAndAnswersCache;
  }
  let rows = [];
  const tenantId = getConfig('tenant-id');
  const url = satisfyApiBaseUrl + '/answers/it/' + tenantId;
  try {
    const response = UrlFetchApp.fetch(url);
    const data = JSON.parse(response.getContentText());
    rows.push([
      'Type', 'Id', 'Title'
    ])
    data.answers.forEach(function (answer) {
      rows.push([
        'question', answer.question.id, answer.question.translations[0].title
      ])
      rows.push([
        'answer', answer.id, answer.translations[0].title
      ])
    })
  } catch (f) {
    Logger.log('Fail fetching ' + url + ' ' + f.message);
    throw new Error(f.message);
  }
  questionsAndAnswersCache = rows;
  return rows;
}

let qestionAnswerTextCache = {};
function getQuestionAnswerText(uuid) {
  if (qestionAnswerTextCache.hasOwnProperty(uuid)) {
    return qestionAnswerTextCache[uuid];
  }
  const rows = getQuestionsAndAnswers();
  let text = uuid;
  rows.forEach(function (row) {
    if (row[1] === uuid) {
      text = row[2];
    }
  })
  qestionAnswerTextCache[uuid] = text;
  return text;
}

function importQuestionsAndAnswers() {
  const rows = getQuestionsAndAnswers();
  const spreadsheet = SpreadsheetApp.getActive();
  let sheet = spreadsheet.getSheetByName(questionsAndAnswersSheetName);
  if (sheet) {
    spreadsheet.setActiveSheet(sheet, true);
    let numRows = rows.length;
    let numColumns = rows[0].length;
    sheet.getRange(1, 1, numRows, numColumns).setValues(rows);
  }
}