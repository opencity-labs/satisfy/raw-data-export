# Satisfy Raw Data Export
## Esporta le valutazioni di satisfy in formato csv

Questo repository contiene le configurazioni per eseguire una pipeline in grado di serializzare in formato csv 
le valutazioni di satisfy salvate in un topic di kafka.

Il servizio `ratings-to-csv` esegue un'istanza di vector.dev configurata con `vector_csv.yaml` in cui:
 - la sorgente è il topic kafka decodificato in json
 - il trasformatore è un remap che appiattisce il json
 - la destinazione è un file csv

Le variabili d'ambiente previste sono:

|Env|Default||
|---|---|---|
|KAFKA_SERVER|kafka:9092|Kafka bootstrap servers separati da virgola|
|KAFKA_GROUP_ID|ratings-to-csv|Kafka consumer group id|
|KAFKA_TOPIC|ratings|Kafka topic da cui leggere|

Il servizio salva i dati nella directory `/csv` che va opportunamente mappata nel filesustem host

Il servizio si aspetta di leggere valutazioni in formato json secondo questo schema:
```json
{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "title": "Satisft rating",
  "type": "object",
  "properties": {
    "X-Forwarded-For": {
      "type": "string"
    },
    "answers": {
      "type": "array",
      "items": {
        "type": "object"        
      }
    },
    "entrypoint-id": {
      "type": "string"
    },
    "expiry": {
      "type": "number"
    },
    "id": {
      "type": "string"
    },
    "meta": {
      "type": "object",
      "properties": {
        "lang": {
          "type": "string"
        },
        "title": {
          "type": "string"
        },
        "uri": {
          "type": "string"
        },
        "user-agent": {
          "type": "string"
        }
      },
      "required": [
        "lang",
        "title",
        "uri",
        "user-agent"
      ]
    },
    "path": {
      "type": "string"
    },
    "review": {
      "type": "string"
    },
    "source_type": {
      "type": "string"
    },
    "timestamp": {
      "type": "string"
    },
    "value": {
      "type": "number"
    },
    "version": {
      "type": "number"
    },
    "widget-version": {
      "type": "string"
    }
  },
  "required": [
    "X-Forwarded-For",
    "entrypoint-id",
    "id",
    "meta",
    "path",
    "source_type",
    "timestamp",
    "value",
    "version",
    "widget-version"
  ]
}
```

Il csv viene generato con i seguenti header:
- `file_id`	Identificatore file di aggregazione
- `timestamp`	Timestamp di inserimento della valutazione
- `id`	Identificativo della valutazione
- `lang`	Lingua usata dall'utente
- `title`	Titolo della pagina su cui l'utente ha dato valutazione
- `uri`	Indirizzo della pagina su cui l'utente ha dato valutazione
- `user_agent`	User agent usato dall'utente
- `review`	Testo inserito dall'utente
- `value`	Valore della valutazione
- `version`	Versione dello schema dati
- `widget_version`  	Versione del widget utilizzata
- `question`	Uuid domanda
- `answer`	Uuid risposta


Il repository mette inoltre a disposizione a scopo di test e sviluppo tutto lo stack applicativo che comprende:
 - `kafka` e `zookeeper` per la gestione e persistenza degli eventi
 - `kafka-ui`: per debug degli eventi
 - `ratings-api`: vector.dev per esporre un endpoint di scrittura su kafka delle valutazioni
 - `main.py` uno script in pyton per inserire un numero arbitrario di valutazioni:
Per installare lo script pyton, eseguire: `pip3 install -r requirements.txt`
Per avviare lo script pyton, eseguire `python3.11 main.py`

