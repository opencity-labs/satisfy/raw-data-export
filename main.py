#!/usr/bin/env python
from datetime import datetime, date, timedelta

import json
import os
import random
from hashlib import md5

import requests

from random import choices

HOST_URI = os.getenv('SATISFY_HOST_URI', default='http://0.0.0.0:8081')
START_DATE = os.getenv('START_DATE', default='2022-01-10')
END_DATE = os.getenv('END_DATE', default='2022-03-18')


def send_rating(sleep_min, sleep_max, rating_value, tenant, entrypoint, title, uri, user_agent, current_date):
    number_of_ratings = random.randint(8, 10)
    for i in range(number_of_ratings):
        obj = {
            "X-Forwarded-For": "1.2.3.4",
            "answers": [
                {
                    "82974aed-f7a7-4d8d-a996-b27a2fb1734b": "1d251225-339b-4d63-a2f3-24392f4461dd"
                }
            ],
            "entrypoint-id": "930b196e-cffa-4832-80cf-2514f5f83816" if entrypoint == 'sdc-www2' else '3fad03ff-1d39-44e7-880f-7a7967b0d915',
            "expiry": 1700256342613,
            "id": f"demo_bot_{md5(bytes(random.randint(1, 10000))).hexdigest()}",
            "meta": {
                "lang": "it-IT",
                "title": "Comunicazione di accensione fuoco per lo smaltimento del materiale vegetale di risulta, derivante da operazioni agricole - Stanza del cittadino",
                "uri": "https://servizi.comune.vicopisano.pi.it/lang/it/pratiche/fdf81843-419f-4242-8fae-c3b6b2350ae0",
                "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36"
            },
            "path": "/",
            "review": "ABILITARE ACCESSO CON CNS",
            "source_type": "http",
            "timestamp": current_date.strftime('%Y-%m-%dT%H:%M:%S.000Z'),
            "value": random.randint(1, 5),
            "version": 1,
            "widget-version": "1.3.0"
        }

        requests.post(HOST_URI, data=json.dumps(obj), timeout=20)
        # print(obj['tenant-name'], "/", obj['entrypoint-name'], ': Sent', obj['value'])


if __name__ == "__main__":
    rating_value = list(range(1, 6))

    # tenant = os.environ.get('TENANT', 'comune-di-genova')
    tenant = ['comune-di-monopoli', 'comune-di-tavagnacco']

    # entrypoint = os.environ.get('ENTRYPOINT', 'segnalaci')
    entrypoint = ['sdc-www2', 'opencity']

    uri_sdc_monopoli = [
        'https://www2.stanzadelcittadino.it/comune-di-monopoli/servizi',
        'https://www2.stanzadelcittadino.it/comune-di-monopoli/pratiche'
    ]
    uri_sdc_tavagnacco = [
        'https://www2.stanzadelcittadino.it/comune-di-tavagnacco/servizi',
        'https://www2.stanzadelcittadino.it/comune-di-tavagnacco/pratiche'
    ]
    uri_opencity_monopoli = [
        'https://www.comune.monopoli.ba.it/Amministrazione',
        'https://www.comune.monopoli.ba.it/Novita'
    ]
    uri_opencity_tavagnacco = [
        'https://www.comune.tavagnacco.ud.it/Amministrazione',
        'https://www.comune.tavagnacco.ud.it/Novita'
    ]
    title_sdc = ['Tutti i servizi', 'Le mie pratiche']
    title_opencity = ['Amministrazione', 'Novita']
    user_agent = [
        'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.3',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/43.4',
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
        'Mozilla/5.0 (iPhone; CPU iPhone OS 11_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) ',
        'Version/10.0 Mobile/14E304 Safari/602.1'
    ]
    sleep_min = int(os.environ.get('SLEEP_MIN', 1))
    sleep_max = int(os.environ.get('SLEEP_MAX', 3))

    start_date = datetime.strptime(START_DATE, '%Y-%m-%d')
    end_date = datetime.strptime(END_DATE, '%Y-%m-%d')
    delta = timedelta(days=1)

    while start_date <= end_date:
        print(str(start_date))

        # tenant_weights = list(np.random.uniform(0, 1, 2))
        # entrypoint_weights = list(np.random.uniform(0, 1, 2))

        selected_tenant = random.choice(tenant)
        selected_entrypoint = random.choice(entrypoint)

        if selected_tenant == 'comune-di-monopoli' and selected_entrypoint == 'sdc-www2':
            send_rating(
                sleep_min,
                sleep_max,
                rating_value,
                selected_tenant,
                selected_entrypoint,
                title_sdc,
                uri_sdc_monopoli,
                user_agent,
                start_date
            )
        elif selected_tenant == 'comune-di-monopoli' and selected_entrypoint == 'opencity':
            send_rating(
                sleep_min,
                sleep_max,
                rating_value,
                selected_tenant,
                selected_entrypoint,
                title_opencity,
                uri_opencity_monopoli,
                user_agent,
                start_date
            )
        elif selected_tenant == 'comune-di-tavagnacco' and selected_entrypoint == 'sdc-www2':
            send_rating(
                sleep_min,
                sleep_max,
                rating_value,
                selected_tenant,
                selected_entrypoint,
                title_sdc,
                uri_sdc_tavagnacco,
                user_agent,
                start_date
            )
        elif selected_tenant == 'comune-di-tavagnacco' and selected_entrypoint == 'opencity':
            send_rating(
                sleep_min,
                sleep_max,
                rating_value,
                selected_tenant,
                selected_entrypoint,
                title_opencity,
                uri_opencity_tavagnacco,
                user_agent,
                start_date
            )
        start_date += delta
